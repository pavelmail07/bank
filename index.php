<?php require('header.php'); ?>
<div class="container">
    <div class="row">
        <div class="col mt-1">
            <?php echo $success_msg; ?>
            <button class="btn btn-success mb-1" data-toggle="modal" data-target="#Modal"><i class="fa fa-user-plus"></i></button>
            <a class="btn btn-primary mb-1" href="mortgage-calculator.php" role="button"><i class="fa fa-calculator" aria-hidden="true"></i>&nbsp;Mortgage calculator</a>
            <table class="table shadow ">
                <thead class="thead-dark">
                    <tr>
                        <th>№</th>
                        <th>Bank Name</th>
                        <th>Interest rate</th>
                        <th>Maximum loan</th>
                        <th>Minimum down payment</th>
                        <th>Loan term</th>
                    </tr>
                    <?php $count = 0;                        

                    foreach ($result as $value) { ?>
                    <tr>
                        <td><?php echo ++$count;?></td>
                        <td><?php echo $value['bank_name']; ?></td>
                        <td><?php echo $value['interest_rate']; ?></td>
                        <td><?php echo $value['max_loan']; ?></td>
                        <td><?php echo $value['min_down_payment']; ?></td>
                        <td><?php echo $value['loan_term']; ?></td>
                        <td>
                            <a href="?edit=<?php echo $value['id']; ?>" class="btn btn-success btn-sm" data-toggle="modal" data-target="#editModal<?php echo $value['id']; ?>"><i class="fa fa-edit"></i></a> 
                            <a href="?delete=<?php echo $value['id']; ?>" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#deleteModal<?php echo $value['id']; ?>"><i class="fa fa-trash"></i></a>
                            <?php require 'modal.php'; ?>
                        </td>
                    </tr> <?php } ?>
                </thead>
            </table>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="Modal">
    <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content shadow">
        <div class="modal-header">
        <h5 class="modal-title">Add bank</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
        <form action="" method="post">
            <div class="form-group">
                <input type="text" class="form-control" name="bank_name" value="" placeholder="Name" required>
            </div>
            <div class="form-group">
                <input type="number" min="1" max="50" class="form-control" name="interest_rate" value="" placeholder="Interest rate" required>
            </div>
            <div class="form-group">
                <input type="number"  min="5000" max="2000000" class="form-control" name="max_loan" value="" placeholder="Maximum loan" required>
            </div>
            <div class="form-group">
                <input type="number"  min="0" max="90" class="form-control" name="min_down_payment" value="" placeholder="Minimum down payment" required>
            </div>
            <div class="form-group">
                <input type="number"  min="1" max="30" class="form-control" name="loan_term" value="" placeholder="Loan term" required>
            </div>                
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="submit" name="submit" class="btn btn-primary">Add</button>
        </div>
        </form>
    </div>
    </div>
</div>
<?php require('footer.php');