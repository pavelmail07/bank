<?php require('header.php'); ?>
<a class="btn btn-primary success mb-1" href="index.php" role="button"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;Back to Bank page</a>

<?php if($result) : ?>
    <form method="POST">
        <div class="form-group col-md-4">
            <label for="inputState">Bank</label>
            <select id="inputState" name="calc_bank_id" class="<?php echo $error_bank_id ? 'is-invalid' : '';?> form-control">
                <option>Choose bank, please</option>
                <?php foreach($result as $bank): ?>
                <option value="<?php echo $bank['id'];?>" <?php echo ($bank['id'] == $calc_bank_id) ? 'selected' : ''; ?>><?php echo $bank['bank_name'];?></option>
                <?php endforeach;?>
            </select>             
        </div>        
        <div class="form-group col-md-6">
            <label for="initial-loan">Initial loan</label>
            <input type="number" value="<?php echo $calc_initial_loan ? $calc_initial_loan : 0;?>" name="calc_initial_loan" class="<?php echo $error_max_loan ? 'is-invalid' : '';?> form-control" id="initial-loan" placeholder="Initial loan">
            <?php if($error_max_loan): ?>
            <div id="validationIitialLoan" class="invalid-feedback">
                Please set loan less than <?php echo $error_max_loan;?>.
            </div>
            <?php endif; ?>        
        </div>
        <div class="form-group col-md-6">
            <label for="down-payment">Down payment</label>
            <input type="number" value="<?php echo $calc_down_payment ? $calc_down_payment : 0;?>" name="calc_down_payment" class="<?php echo $error_min_down_payment ? 'is-invalid' : '';?> form-control" id="down-payment" placeholder="Down payment">
            <?php if($error_min_down_payment): ?>
            <div id="validationDownPayment" class="invalid-feedback">
                Please set down payment more than <?php echo $error_min_down_payment;?>.
            </div>
            <?php endif; ?>    
        </div>            
        <button type="submit" value="calc_submit" name="calc_submit" class="btn btn-primary">Submit</button>
    </form>
<?php else: ?>
    <div class="alert alert-warning" role="alert">
        Please, add banks on bank page!
    </div>
<?php endif; ?>

<?php if($monthly_payment): ?>
    <div class="alert alert-success" role="alert">
        Your monthly payment is <?php echo $monthly_payment; ?>
    </div>
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Monthly payment</th>
            </tr>
        </thead>
        <tbody>
            <?php for($i=1; $i<=$bank_loan_term * 12; $i++): ?>
                <tr>
                    <th scope="row"><?php echo $i; ?></th>
                    <td><?php echo $monthly_payment; ?></td>
                </tr>
            <?php endfor;?>            
        </tbody>
    </table>
<?php endif;?>

<?php require('footer.php');