<?php 

include 'config.php';

$bank_name = isset($_POST['bank_name']) ? $_POST['bank_name'] : '';
$interest_rate = isset($_POST['interest_rate']) ? $_POST['interest_rate'] : '';
$max_loan = isset($_POST['max_loan']) ? $_POST['max_loan'] : '';
$min_down_payment = isset($_POST['min_down_payment']) ? $_POST['min_down_payment'] : '';
$loan_term = isset($_POST['loan_term']) ? $_POST['loan_term'] : '';

// Read
$sql = $pdo->prepare("SELECT * FROM `banks`");
$sql->execute();
$result = $sql->fetchAll();

// Create
if (isset($_POST['submit'])) {
	$sql = ("INSERT INTO `banks`(`bank_name`, `interest_rate`, `max_loan`, `min_down_payment`, `loan_term`) VALUES(?,?,?,?,?)");
	$query = $pdo->prepare($sql);
	$query->execute([$bank_name, $interest_rate, $max_loan, $min_down_payment, $loan_term]);
    header('Location: '. $_SERVER['HTTP_REFERER']);	
}

// Update
$edit_submit = isset($_POST['edit_submit']) ? $_POST['edit_submit'] : '';
$edit_bank_name = isset($_POST['edit_bank_name']) ? $_POST['edit_bank_name'] : '';
$edit_interest_rate = isset($_POST['edit_interest_rate']) ? $_POST['edit_interest_rate'] : '';
$edit_max_loan = isset($_POST['edit_max_loan']) ? $_POST['edit_max_loan'] : '';
$edit_min_down_payment = isset($_POST['edit_min_down_payment']) ? $_POST['edit_min_down_payment'] : '';
$edit_loan_term = isset($_POST['edit_loan_term']) ? $_POST['edit_loan_term'] : '';
$get_id = isset($_GET['id']) ? $_GET['id'] : '';

if (isset($_POST['edit_submit'])) {
	$upd_sql = "UPDATE banks SET bank_name=?, interest_rate=?, max_loan=?, min_down_payment=?, loan_term=?  WHERE id=?";
	$upd_query = $pdo->prepare($upd_sql);
	$upd_query->execute([$edit_bank_name, $edit_interest_rate, $edit_max_loan, $edit_min_down_payment, $edit_loan_term, $get_id]);
	header('Location: '. $_SERVER['HTTP_REFERER']);
}

// DELETE
if (isset($_POST['delete_submit'])) {
	$sql = "DELETE FROM banks WHERE id=?";
	$query = $pdo->prepare($sql);
	$query->execute([$get_id]);
	header('Location: '. $_SERVER['HTTP_REFERER']);
}


// Validation
if (isset($_POST['calc_submit'])) {
	$calc_bank_id = isset($_POST['calc_bank_id']) ? (int) $_POST['calc_bank_id'] : '';
	$calc_initial_loan = isset($_POST['calc_initial_loan']) ? (float) $_POST['calc_initial_loan'] : '';
	$calc_down_payment = isset($_POST['calc_down_payment']) ? (float) $_POST['calc_down_payment'] : '';
	
	$error_bank_id = false;
	$error_max_loan = false;
	$error_min_down_payment = false;
	$monthly_payment = false;

	if($calc_bank_id){
		$calc_sql = "SELECT * FROM `banks` WHERE id=?";
		$calc_query = $pdo->prepare($calc_sql);
		$calc_query->execute([$calc_bank_id]);
		$calc_result = $calc_query->fetchAll();
		
		$bank_max_loan =  (float) $calc_result[0]['max_loan'];
		$bank_min_down_payment =  (float) $calc_result[0]['min_down_payment'];
		$bank_interest_rate =  (float) $calc_result[0]['interest_rate'];
		$bank_loan_term =  (float) $calc_result[0]['loan_term'];	

		if($calc_initial_loan > $bank_max_loan){
			$error_max_loan = $bank_max_loan;
		}

		if($calc_down_payment < $bank_min_down_payment){
			$error_min_down_payment = $bank_min_down_payment;
		}

	}

	else{
		$error_bank_id = true;
	}

	if(!$error_bank_id && !$error_max_loan && !$error_min_down_payment){
		$monthly_payment = round( $calc_initial_loan * ($bank_interest_rate / (100 * 12)) * pow((1 + $bank_interest_rate / (100 * 12)), $bank_loan_term * 12 ) / (pow(( 1 + $bank_interest_rate / (100 * 12)), $bank_loan_term * 12 ) - 1), 2);
	} 

}