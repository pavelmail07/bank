<!-- Modal Edit-->
<div class="modal fade" id="editModal<?php echo $value['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content shadow">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit bank: <?php echo $value['bank_name']; ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="?id=<?php echo $value['id']; ?>" method="post">
            <div class="form-group">
                <input type="text" class="form-control" name="edit_bank_name" value="<?php echo $value['bank_name']; ?>" placeholder="Name" required>
            </div>
            <div class="form-group">
                <input type="number" min="1" max="50" class="form-control" name="edit_interest_rate" value="<?php echo $value['interest_rate']; ?>" placeholder="Interest rate" required>
            </div>
            <div class="form-group">
                <input type="number"  min="5000" max="2000000" class="form-control" name="edit_max_loan" value="<?php echo $value['max_loan']; ?>" placeholder="Maximum loan" required>
            </div>
            <div class="form-group">
                <input type="number"  min="0" max="90" class="form-control" name="edit_min_down_payment" value="<?php echo $value['min_down_payment']; ?>" placeholder="Minimum down payment" required>
            </div>
            <div class="form-group">
                <input type="number"  min="1" max="30" class="form-control" name="edit_loan_term" value="<?php echo $value['loan_term']; ?>" placeholder="Loan term" required>
            </div>   	
        	<div class="modal-footer">
        		<button type="submit" name="edit_submit" class="btn btn-primary">Update</button>
        	</div>
        </form>	
      </div>
    </div>
  </div>
</div>

<!-- DELETE MODAL -->
<div class="modal fade" id="deleteModal<?php echo $value['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content shadow">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Delete bank - <?php echo $value['bank_name']; ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <form action="?id=<?php echo $value['id']; ?>" method="post">
        	<button type="submit" name="delete_submit" class="btn btn-danger">Delete</button>
    	</form>
      </div>
    </div>
  </div>
</div>
